# OM2

This crate is a loose attempt at creating a `Unit` struct based off of the units
available in the [OM2][1] ontology. It also exports `Measure` and `NumericUnion`
classes. 

All structs/enums exported are (de)serializable via Serde.

```rust
use om2::{Unit, Measure, NumericUnion};
let measure = Measure::builder()
    .has_unit(Unit::Hour)
    .has_numerical_value(NumericUnion::Integer(7))
    .build().unwrap();
assert_eq!(measure.has_unit(), &Unit::Hour);
assert_eq!(measure.has_numerical_value(), &NumericUnion::Integer(7));
```


[1]: https://github.com/HajoRijgersberg/OM

